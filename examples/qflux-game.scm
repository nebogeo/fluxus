; qflux 
; GPL licence (c) dave griffiths 2006
; turn all the squares green, avoid the red dudes
; sort of influenced by q*bert

; keys:
; q : turn left
; e : turn right
; spacebar : move forward

;~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

(struct block (height obj col) #:mutable)

(define (make-block height)
    (block height 0 (vector 1 1 1)))
    
(define (block-set-col! blk col)
    (let ((obj (block-obj blk)))
    (let ((newsquare (not (equal? (block-col blk) col))))
    (set-block-col! blk col)
    (cond ((not (zero? obj)) ; check we've built the object
        (grab obj)
        (colour col)
        (ungrab)))
    newsquare)))

(define (block-build! block)
    (push)
    (wire-colour (vector 0 0 0))
    (translate (vector 0 (block-height block) 0))
    (set-block-obj! block (build-cube))
    (pop))
    
(define (block-destroy block)
    (destroy (block-obj block)))

;~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

(struct world (width height entities vblocks) #:mutable)

(define (make-world width height f)
    (define (_make n v)
        (vector-set! v n (make-block (f n)))
        (if (zero? n)
            v
            (_make (- n 1) v)))
    (world width height '()
        (_make (- (* width height) 1) (make-vector (* width height) '()))))

(define (world-get-block world pos)
    (vector-ref (world-vblocks world)
        (+ (vector-ref pos 0) (* (vector-ref pos 1)
            (world-width world)))))

(define (world-set-block! world pos block)
    (vector-set! (world-vblocks world)
        (+ (vector-ref pos 0) (* (vector-ref pos 1)
            (world-width world))) block))

(define (world-build! world)
    (define (__build x y)
        (translate (vector 0 0 -1))
        (block-build! (world-get-block world (vector x y)))
        (if (zero? y)
            0
            (__build x (- y 1))))
    (define (_build x y)
        (translate (vector -1 0 0))
        (push)
        (__build x y)
        (pop)
        (if (zero? x)
            0
            (_build (- x 1) y)))
    (push)
    (line-width 4)
    (hint-wire)
    (translate (vector (world-width world) 0 (world-height world)))
    (_build (- (world-width world) 1) (- (world-height world) 1))
    (pop))
    
(define (world-destroy world)
    (define (dest n w)
        (block-destroy (vector-ref w n))
        (if (zero? n)
            0
            (dest (- n 1) w)))
    (dest (- (vector-length (world-vblocks world)) 1) (world-vblocks world)))
    
;~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

(struct entity (type pos dir obj col) #:mutable)

(define (make-entity type x y direction)
    (list type (vector x y) direction 0 (vector 1 1 1)))
    
(define entity-size 5)

(define (entity-rot-cw! entity world)
    (let ((dir (entity-dir entity)))
    (set! dir (+ dir 1))
    (when (> dir 3)
        (set! dir 0))
    (set-entity-dir! entity dir)
    (entity-update-obj entity world)))

(define (entity-rot-ccw! entity world)
    (let ((dir (entity-dir entity)))
    (set! dir (- dir 1))
    (when (< dir 0)
        (set! dir 3))
    (set-entity-dir! entity dir)
    (entity-update-obj entity world)))

(define (entity-move! entity world)
    (let ((pos (entity-pos entity)) (dir (entity-dir entity)))
    ; copy vector elements to numbers as the vector is a reference
    (let ((px (vector-ref pos 0))(py (vector-ref pos 1)))

    (cond ((eq? dir 0) (set! py (- py 1)))
    ((eq? dir 1) (set! px (+ px 1)))
    ((eq? dir 2) (set! py (+ py 1)))
    ((eq? dir 3) (set! px (- px 1))))
    
    ; wrap to the to the world limits
    (when (< px 0) (set! px 0))
    (when (< py 0) (set! py 0))
    (when (>= px (world-width world))  (set! px (- (world-width world) 1)))
    (when (>= py (world-height world)) (set! py (- (world-height world) 1)))
    
    (let ((block (world-get-block world (vector px py))))
    (cond (; if the gradient isn't too high            
            (< (abs (- (block-height block)
                 (block-height (world-get-block world pos)))) 2)
        (set-entity-pos! entity (vector px py))
        (entity-update-obj entity world)
        (when (string=? (entity-type entity) "p")
            (block-set-col! (world-get-block world (vector px py)) (vector 0 1 0)))))))))

(define (entity-update-obj entity world)
    (let ((obj (entity-obj entity)))
    (let ((pos (entity-pos entity)))
    (let ((height (block-height (world-get-block world pos))))
    (cond ((not (zero? obj)) ; check we've built the object
        (grab obj)
        (identity)
        (translate (vector (vector-ref pos 0) (+ height 1) (vector-ref pos 1)))
        (rotate (vector 0 (* (entity-dir entity) -90) 0))
        (scale (vector 0.5 0.5 0.5))
        (ungrab)))))))

(define (entity-build! entity)
    (push)
    (colour (entity-col entity))
    (specular (vector 1 1 1))
    (shinyness 20)

    ; main body
    (set-entity-obj! entity (build-sphere 15 15))
    (parent (entity-obj entity))

    ; nose
    (push)
    (translate (vector 0 -0.2 -1))
    (scale (vector 0.3 0.3 0.3))
    (build-sphere 5 5)
    (pop)

    ; eye 1
    (push)
    (translate (vector 0.3 0 -0.9))
    (scale (vector 0.3 0.3 0.3))
    (colour (vector 1 1 1))
    (build-sphere 3 3)

    (translate (vector 0 0 -0.5))
    (scale (vector 0.5 0.5 0.5))
    (colour (vector 0 0 0))
    (build-sphere 3 3)    
    (pop)

    ; eye 2
    (push)
    (translate (vector -0.3 0 -0.9))
    (scale (vector 0.3 0.3 0.3))
    (colour (vector 1 1 1))
    (build-sphere 3 3)

    (translate (vector 0 0 -0.5))
    (scale (vector 0.5 0.5 0.5))
    (colour (vector 0 0 0))
    (build-sphere 3 3)    
    (pop)
    (pop))

(define (entity-destroy entity)
    (destroy (entity-obj entity)))

;~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

(struct player entity (squarecount) #:mutable)

(define (make-player x y direction)
    (player "p" (vector x y) direction 0 (vector 0 1 0) 0))

(define (player-dec-squarecount! entity)
    (set-player-squarecount! entity (- (player-squarecount entity) 1)))

(define key-release 0)

(define (player-update player world game)

    (define (enemy-check enemies)
        (if (null? enemies) 
            0
            (cond 
                ((equal? (entity-pos (car enemies)) (entity-pos player))
                    1)
                (else 
                    (if (null? (cdr enemies))
                        0
                        (enemy-check (cdr enemies)))))))

    (cond ((eq? 1 (enemy-check (level-enemies (game-get-current-level game))))
        (set-game-restart! game 1)))

    (cond ((zero? (player-squarecount player))
        (set-game-next-level! game 1)))
    (cond
        ((key-pressed "e")
            (when (eq? key-release 0)
                (entity-rot-cw! player world))
            (set! key-release 1))
                
        ((key-pressed "q")
            (when (eq? key-release 0)
                (entity-rot-ccw! player world))
            (set! key-release 1))
                
        ((key-pressed " ")
            (when (eq? key-release 0)
                (cond ((eq? #t (entity-move! player world))
                    (player-dec-squarecount! player))))
            (set! key-release 1))
                
          (else
              (set! key-release 0))))

;~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

(struct enemy entity (tick-rate last-tick program pc) #:mutable)

(define (make-enemy x y direction speed program)
    (enemy "e" (vector x y) direction 0 (vector 1 0 0) speed 0.0 program 0))

(define (enemy-inc-pc enemy)        
    (set-enemy-pc! enemy (modulo (+ (enemy-pc enemy) 1)
        (vector-length (enemy-program enemy)))))
        
(define (enemy-update entity world)
    (cond ((> (time) (+ (enemy-last-tick entity)
                        (enemy-tick-rate entity)))
        (enemy-inc-pc entity)
        (let ((t (vector-ref (enemy-program entity) (enemy-pc entity))))
        (cond
            ((eq? t 0) (entity-move! entity world))
            ((eq? t 1) (entity-rot-ccw! entity world))                
            ((eq? t 2) (entity-rot-cw! entity world))))            
        (set-enemy-last-tick! entity (time)))))

;~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

(struct level (size landscape camera player enemies world) #:mutable)

(define (make-level size landscape camera player enemies)
    (level size landscape camera player enemies 0))

(define (level-squarecount level)
    (- (* (vector-ref (level-size level) 0) (vector-ref (level-size level) 1)) 1))

(define (level-build level)
    (define (update-enemies e)
        (entity-build! (car e))
        (entity-update-obj (car e) (level-world level))
        (if (null? (cdr e))
            0
            (update-enemies (cdr e))))


    (let ((dim (level-size level)))
    (set-level-world! level (make-world (vector-ref dim 0) (vector-ref dim 1)
        (lambda (n) (vector-ref (level-landscape level) n))))
    (world-build! (level-world level))
    (entity-build! (level-player level))
    (entity-update-obj (level-player level) (level-world level))

    (set-player-squarecount! (level-player level) (level-squarecount level))
     
     ; set the first block to green
     (block-set-col! (world-get-block (level-world level)
          (entity-pos (level-player level)))
               (vector 0 1 0))
     ;(lock-camera (entity-obj (level-player level)))
     (set-camera-transform (minverse (level-camera level)))
     
    (when (not (eq? '() (level-enemies level)))
          (update-enemies (level-enemies level)))))

(define (level-destroy level)
    (define (destroy-enemies e)
        (entity-destroy (car e))
        (if (null? (cdr e))
            0
            (destroy-enemies (cdr e))))
    (when (not (eq? '() (level-enemies level)))
          (destroy-enemies (level-enemies level)))
    (world-destroy (level-world level))
    (entity-destroy (level-player level)))

(define (level-update level game)
    (define (update-enemies e)
        (enemy-update (car e) (level-world level))
        (if (null? (cdr e))
            0
            (update-enemies (cdr e))))
        
    (when (not (eq? '() (level-enemies level)))
          (update-enemies (level-enemies level)))
    (player-update (level-player level) (level-world level) game))

;~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

(struct game (level-no levels next-level restart) #:mutable)

(define (make-game levels)
    (game 0 levels 0 0))

(define (game-inc-level-no! game)
    (set-game-level-no! game (modulo (+ (game-level-no game) 1) 
        (length (game-levels game)))))

(define (game-get-current-level game)
    (list-ref (game-levels game) (game-level-no game)))

(define (game-build game)
    (level-build (game-get-current-level game)))

(define (game-update game)
    (cond ((eq? (game-next-level game) 1)
        (set-game-next-level! game 0)
        (level-destroy (game-get-current-level game))
        (game-inc-level-no! game)
        (level-build (game-get-current-level game))))

    (cond ((eq? (game-restart game) 1)
        (set-game-restart! game 0)
        (level-destroy (game-get-current-level game))
        (level-build (game-get-current-level game))))
        
    (level-update (game-get-current-level game) game))


;~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

(clear)
(clear-colour (vector 0 0.4 0.8))
(ortho)
(set-ortho-zoom 10)

(define game (make-game (list

     (make-level
            (vector 3 3)
            (vector 1 1 1
                    1 1 1
                    1 1 1 )
            (mmul
                    (mrotate (vector 45 160 -200))
                    (mtranslate (vector 1 0 15 )))
            (make-player 1 1 0)
            (list)) 
            
     (make-level
            (vector 5 5)
            (vector 1 1 1 1 1
                    4 5 5 5 2
                    4 8 9 6 2
                    4 7 7 6 2
                    3 3 3 3 2 )
            (mmul
                    (mrotate (vector -42 35 209))
                    (mtranslate (vector 0 -1 15)))
            (make-player 0 0 2)
            (list))

      (make-level
            (vector 5 5)
            (vector 1 1 1 1 1
                    1 2 2 2 1
                    1 2 3 2 1
                    1 2 2 2 1
                    1 1 1 1 1 )
            (mmul
                    (mrotate (vector -45 -45 145))
                    (mtranslate (vector -2 -2 10)))
            (make-player 2 2 3)
            (list
                (make-enemy 0 4 0 0.5 (vector 0 0 0 0 1))))        

 
    (make-level
            (vector 7 7)
            (vector 3 3 3 2 3 3 3
                    3 3 3 1 3 3 3
                    3 3 3 1 3 3 3
                    2 1 1 1 1 1 2
                    3 3 3 1 3 3 3
                    3 3 3 1 3 3 3
                    3 3 3 2 3 3 3 )
            (mmul
                    (mrotate (vector 240 45 -50))
                    (mtranslate (vector 4 -2 10)))
            (make-player 1 1 0)
            (list
                (make-enemy 0 0 0 0.5 (vector 0 0 1))
                (make-enemy 0 6 0 0.75 (vector 0 1 0))
                (make-enemy 6 0 0 0.6 (vector 1 0 0))
                (make-enemy 6 6 0 0.4 (vector 0 0 1))))        
                
  (make-level
            (vector 10 10)
            (vector 5 6 6 7 7 8 8 9 9 10
                    5 5 6 6 7 7 8 8 9 9
                    4 5 5 6 6 7 7 8 8 9
                    4 4 5 5 6 6 7 7 8 8
                    3 4 4 5 5 6 6 7 7 8
                    3 3 4 4 5 5 6 6 7 7
                    2 3 3 4 4 5 5 6 6 7
                    2 2 3 3 4 4 5 5 6 6
                    1 2 2 3 3 4 4 5 5 6
                    1 1 2 2 3 3 4 4 5 5)
            (mmul
                    (mrotate (vector 55 205 205))
                    (mtranslate (vector 2 -8 100)))
            (make-player 0 0 0)
            (list
                (make-enemy 4 4 2 0.6 (vector 1 0))
                (make-enemy 3 5 2 0.5 (vector 0 0 1 0))
                (make-enemy 2 4 2 0.4 (vector 0 0 0 0 1 0))
                (make-enemy 1 3 2 0.3 (vector 0 0 0 0 0 0 1 0))
                (make-enemy 0 2 2 0.2 (vector 0 0 0 0 0 0 0 0 1 0))))
                
  (make-level
            (vector 10 10)
            (vector 9 9 9 9 9 9 9 9 9 9
                    9 9 9 1 1 1 1 9 9 9
                    9 9 9 1 1 1 1 9 9 9
                    8 1 1 1 4 4 1 1 1 9 
                    7 2 1 4 4 4 4 1 1 9
                    6 3 1 4 4 4 4 1 1 9
                    5 4 4 4 4 4 1 1 1 9
                    9 9 9 1 1 1 1 9 9 9
                    9 9 9 1 1 1 1 9 9 9
                    9 9 9 9 9 9 9 9 9 9)
            (mmul 
                    (mrotate (vector 0 90 0))
                (mmul
                    (mrotate (vector 200 30 -10))
                    (mtranslate (vector -1 -7 100)))
                    )
            (make-player 4 5 0)
            (list
                (make-enemy 0 0 1 0.1 (vector 
        0 0 0 0 0 0 0 0 0 0 2 0 0 0 0 0 0 0 0 0 0 2 0 0 0 0 0 0 0 0 0 0 2 2
        0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 0 0 2 2))
                (make-enemy 7 1 1 0.5 (vector 2 0 2 0))
                (make-enemy 1 7 1 0.5 (vector 2 0 2 0))
                (make-enemy 7 7 1 0.5 (vector 0 2 0 2))
                (make-enemy 3 1 1 0.3 (vector 0 0 0 2 0 2))
                (make-enemy 4 5 1 0.5 (vector 0 0 2))
            ))
               
   
            
    )))

(game-build game)

(define (render)
    (game-update game))

(every-frame (render))
